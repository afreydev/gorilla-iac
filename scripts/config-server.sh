#!/bin/bash
#Install required software
yum update -y
yum install -y git
yum install -y java-1.8.0-openjdk.x86_64
yum install -y docker
#config java version
alternatives --set java /usr/lib/jvm/jre-1.8.0-openjdk.x86_64/bin/java
#install jenkins
wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
yum install -y jenkins
usermod -a -G docker jenkins
#Start services
service jenkins start
chkconfig jenkins on
service docker start
chkconfig docker on
#create storage directories
mkdir /mnt/data
mkdir /mnt/data/registry
chmod -R 777 /mnt/data
#Start local registry container
docker run -d -p 5000:5000 --restart=always --name registry -v /mnt/data/registry:/var/lib/registry registry:2

